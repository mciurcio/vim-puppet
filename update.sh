#!/bin/bash

echo "Updating pathogen..."
curl -LSso vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim || exit

echo "Updating syntastic..."
curl -LSso /tmp/syntastic-master.zip https://github.com/vim-syntastic/syntastic/archive/master.zip || exit
unzip -d /tmp /tmp/syntastic-master.zip || exit

rm -rf vim/bundle/syntastic
mv /tmp/syntastic-master vim/bundle/syntastic

echo "Updating tabular..."
curl -LSso /tmp/tabular-master.zip https://github.com/godlygeek/tabular/archive/master.zip || exit
unzip -d /tmp /tmp/tabular-master.zip || exit

rm -rf vim/bundle/tabular
mv /tmp/tabular-master vim/bundle/tabular

echo "Updating puppet-git-hooks..."

curl -LSso /tmp/puppet-git-hooks-master.zip  https://github.com/drwahl/puppet-git-hooks/archive/master.zip || exit
unzip -d /tmp /tmp/puppet-git-hooks-master.zip || exit

rm -rf puppet-git-hooks
mv /tmp/puppet-git-hooks-master puppet-git-hooks

